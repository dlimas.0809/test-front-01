import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { CommonModule } from '@angular/common';
import { AdminComponent } from 'src/app/modules/admin/admin.component';

const routes: Routes = [
    {
      path: 'login',
      loadChildren: () => import('../app/modules/login/login.module').then(m=>m.LoginModule) 
    },
    {
      path: 'admin',
      component: AdminComponent,
      children: [
        {
          path: 'consulta',
          loadChildren : ()=>import('../app/modules/consulta/consulta.module').then(m=>m.ConsultaModule)
        }
      ]
    },
    {path: '**', redirectTo: '/404'}
  ];
  
  @NgModule({
    imports: [
      CommonModule,
      RouterModule.forRoot(routes)
    ],
    exports: [
      RouterModule
    ]
  })
  export class RoutesModule { }