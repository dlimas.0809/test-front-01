export class ResponseConsultaRuc {

    ruc: String;
    razonSocial: String;
    estado: String;
    direccion : String;
    ubigeo: String;
    departamento: String;
    provincia : String;
    distrito: String;

}
