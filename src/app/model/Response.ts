// tslint:disable-next-line:no-namespace
declare namespace ResponseNamespace {

    export interface ConsultaRuc {
        username: string;
        password: string;
      }
    
      export interface Response{
        consultaRuc : ConsultaRuc;
      }

}