// tslint:disable-next-line:no-namespace
declare namespace RequestNamespace {

export interface RequestLogin {
    username: string;
    password: string;
  }


  export interface RequestConsultaRuc {
    ruc: string;
  }

  export interface Request{
    requestLogin: RequestLogin;
    requestConsultaRuc : RequestConsultaRuc;
  }

}