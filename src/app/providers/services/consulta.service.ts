import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConsultasEndpoint } from '../endpoint/consultas.endpoint';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService } from './storage.service';
import { STORAGE_KEYS } from 'src/app/constants/general.constant';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService {

  constructor(
    private httpClient: HttpClient,
    private storageService : StorageService
  ) { }

  consultaRuc(ruc : any): Observable<any>{
    let token = this.storageService.getStorage(STORAGE_KEYS.userToken);
    let header=  new HttpHeaders();
    header = header.append('Authorization', 'Bearer '+  token);
    console.log("token " + token)
    return this.httpClient.get<any>(ConsultasEndpoint.consultaOperaciones+ruc, {headers:  header});
  }
}
