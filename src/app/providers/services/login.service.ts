import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestLogin } from 'src/app/model/request-login';
import { Observable } from 'rxjs';

export class OptionsRequest {
  headers?: HttpHeaders | {
    [header: string]: string | Array<string>;
  };
  observe?: 'body';
  params?: {};
  responseType?: 'json';
}


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private httpClient: HttpClient
  ) { }

  autenticar(request: RequestLogin): Observable<any>{
    console.log("llegue?" + JSON.stringify(request))
    const option = new OptionsRequest();
    return this.httpClient.post<any>("http://localhost:8083/auth/signin", request);
  }

 
}
