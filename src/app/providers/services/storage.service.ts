import { Injectable } from '@angular/core';

export interface IStorage {
  typeStorage: string;
  getItem(key: string): any;
  getItemObject<T>(key: string): T;
  removeItem(key: string): void;
  setItem<T = string>(key: string, data: T): void;
  clear(): void;
}

export class SessionStorage implements IStorage {
  typeStorage = 'session';

  getItem(key: string): any {
    return sessionStorage.getItem(key);
  }

  getItemObject<T>(key: string): T {
    return JSON.parse(sessionStorage.getItem(key)) as T;
  }

  removeItem(key: string): void {
    sessionStorage.removeItem(key);
  }

  setItem<T = string>(key: string, data: T): void {
    const type = typeof data;
    const value = type === 'object' ? JSON.stringify(data) : data.toString();
    sessionStorage.setItem(key, value);
  }

  clear(): void {
    sessionStorage.clear();
  }
}


@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private typeStorage =  'session';
  private storage : IStorage;

  constructor() { 
    this.setStorage();
  }

  private setStorage(): void {
    switch (this.typeStorage) {
      case 'session':
        this.storage = new SessionStorage();
        break;
      default:
        this.storage = new SessionStorage();
        break;
    }
  }

  get(): IStorage {
    return this.storage;
  }

  getItem(key: string): any {
    return this.storage.getItem(key);
  }

  getItemObject<T>(key: string): T {
    return this.storage.getItemObject<T>(key);
  }

  removeItem(key: string): void {
    this.storage.removeItem(key);
  }

  setItem<T = string>(key: string, data: T): void {
    this.storage.setItem<T>(key, data);
  }

  clear(): void {
    this.storage.clear();
  }

  saveStorage(key: string, data: any): void {
    const dataTmp = btoa(JSON.stringify(data));
    this.setItem(key, dataTmp);
  }

  getStorage(key: string): any {
    const itemStorage = this.getItem(key);
    return itemStorage != null ? JSON.parse(atob(itemStorage)) : null;
  }

}
