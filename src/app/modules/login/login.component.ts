import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/providers/services/login.service';
import { RequestLogin } from 'src/app/model/request-login';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { STORAGE_KEYS } from 'src/app/constants/general.constant';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/providers/services/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  registerForm : FormGroup;
  userNameControl : AbstractControl;
  passwordControl : AbstractControl;

  constructor(
    private loginService : LoginService,
    private formBuilder : FormBuilder,
    private storageService : StorageService,
    private routerService : Router
  ) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      username : ['', Validators.required],
      password : ['', Validators.required]
    });
    this.userNameControl = this.registerForm.controls.username;
    this.passwordControl = this.registerForm.controls.password;
    this.storageService.clear();
  }

  onSubmit(){
    
    let request = {} as RequestLogin;
    request.username = this.userNameControl.value;
    request.password = this.passwordControl.value;
        this.loginService.autenticar(request).subscribe(data => {
      this.storageService.saveStorage(STORAGE_KEYS.userToken, data.token);
      this.storageService.saveStorage(STORAGE_KEYS.userId, data.username);

      console.log('resultado => ', data.username);
      console.log('resultado => ', data.token);
      this.routerService.navigate(["/admin/consulta"]);
    }, (e) => {
      console.log('Ocurrio un error => ', e);
      });
  }

}
