import { Component, OnInit, NgModule } from '@angular/core';
import { ConsultaService } from 'src/app/providers/services/consulta.service';
import { StorageService } from 'src/app/providers/services/storage.service';
import { STORAGE_KEYS } from 'src/app/constants/general.constant';
import { ResponseConsultaRuc } from 'src/app/model/response-consulta-ruc';
import { AnonymousSubject } from 'rxjs/internal/Subject';
import { Router } from '@angular/router';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {

  ruc: any;
  token : any;
  responseConsulta = new ResponseConsultaRuc();
  noExisteInformacion : any;

  constructor(
    private consultaService : ConsultaService,
    private storageService : StorageService,
    private routerService : Router
  ) { 
  }

  ngOnInit(): void {
    this.token = this.storageService.getStorage(STORAGE_KEYS.userToken);  
    if(this.token == null)
    this.routerService.navigate(["/login"]);
    console.log("token en consulta  " + this.token)
  }

  buscarRuc(){
  this.
    consultaService.consultaRuc(this.ruc)
    .subscribe(data => {
      let ruc = data.ruc;
      console.log("existe infor " + data.razon_social);
      if(ruc != null){
        this.responseConsulta.ruc = data.ruc;
        this.responseConsulta.razonSocial = data.razon_social;
        this.responseConsulta.estado = data.estado;
        this.responseConsulta.direccion = data.direccion;
        this.responseConsulta.ubigeo = data.ubigeo;
        this.responseConsulta.departamento = data.departamento;
        this.responseConsulta.provincia = data.provincia;
        this.responseConsulta.distrito = data.distrito;
      } else{
        this.noExisteInformacion = data.razon_social;
      }
    }, (e) => {
      this.routerService.navigate(["/login"]);
    })
  }



}
