
export const GENERAL = {
  info: 'example'
};

export const STORAGE_KEYS = {
  userToken: 'USER_TOKEN',
  userInfo: 'USER_INFO',
  userId: 'USER'
};

export const GENERAL_LANGS = {
  notifications: {
    success: 'Se realiz\u00f3; correctamente',
    notSaveCorrectly: 'No se guardo correctamente',
    completeFields: 'Completa los campos',
    existIncorrectFields: 'Existen campos incorrectos',
    thereAreChangesWithoutSaving: 'Existen cambios sin guardar'
  }
};


export const DEFAULT_FILTERS = {
  YEAR_INTERVAL: 10,
  DOCUMENT_NUMBER_SIZE: 8,
  DOCUMENT_TYPE: '1',
  OPERATIVE_SYSTEM: '1,2',
  TOKEN_STATUS: '1,2,3',
  OPERATION_STATUS: '0,1,2',
  FIRST_PAGE: 1,
  PAGE_SIZE: 10,
  REGEX: '^[0-9]+$'
};

export const CP_DEFAULT = {
  SINCE_TIME: '000000',
  SINCE_DATE_FORMAT: 'ddMMyyyy',
  UNTIL_DATE_FORMAT: 'ddMMyyyy',
  UNTIL_TIME_FORMAT: 'hhmmss',
  AFFILIATION_DATE_FORMAT: 'DDMMYYYYhhmmss',
  USER: 'PRUEBA12'
}

export const CP_ERROR_CODE = {
  INVALID_DOCUMENT_NUMBER: '1104'
};

export const CP_OK_CODE = '0000';
