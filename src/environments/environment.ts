// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  backUrl: 'http',
  backEndUtil: 'http://localhost:8083',
  appCode: 36,
  moduleCodeReporte: 89,
  formatTypeReporte: '01',
  archiveCodeOperations: 'REP_OPE_APP',
  archiveCodeAppAffiliation: 'REP_AFI_APP',
  archiveCodeTokenAffiliation: 'REP_AFI_TOK',
  serviceTimeout: 30 * 1000,
  showConsoleMessages: true
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
